# frozen_string_literal: true

require 'spec_helper'

context 'When installed' do
  describe package('hitch') do
    it { should be_installed }
  end

  describe service('hitch') do
    it { is_expected.not_to be_running }
  end

  describe file('/var/lib/hitch') do
    it { is_expected.to be_directory }
    it { is_expected.to be_owned_by '_hitch' }
    it { is_expected.to be_grouped_into '_hitch' }
  end
end
