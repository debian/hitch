# frozen_string_literal: true

require 'spec_helper'
require 'open3'

hitch_conf = <<~EOF
  frontend = {
    host = "*"
    port = "443"
  }

  backend = "[::1]:8080"

  pem-file = {
    cert = "/etc/hitch/cert.pem"
    private-key = "/etc/hitch/key.pem"
  }
EOF

context 'When configuration and certificate is present' do
  before do
    _stdout, _stderr, _status = Open3.capture3(
      'openssl', 'req', '-x509',
      '-newkey', 'rsa:4096',
      '-sha256', '-nodes',
      '-subj', '/CN=localhost',
      '-days', '1',
      '-keyout', '/etc/hitch/key.pem',
      '-out', '/etc/hitch/cert.pem'
    )

    File.write('/etc/hitch/hitch.conf', hitch_conf)
  end

  describe command('service hitch start') do
    its(:exit_status) { should be 0 }
  end

  describe service('hitch') do
    it { is_expected.to be_running }
  end
end
